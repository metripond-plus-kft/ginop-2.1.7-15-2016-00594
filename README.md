# GINOP-2.1.7-15-2016-00594
Automatikus működésű hiteles digitális adagoló kiszerelő mérleg fejlesztése

# A projekt tartalmának bemutatása:
A K+F projekt keretében egy olyan automatikus működésű hiteles digitális adagoló kiszerelő mérleg prototípusának elkészítését valósítjuk meg, mely a legkülönbözőbb ömlesztett termények, termékek roncsolódásmentes kiszerelését és hiteles mérését teszi lehetővé zsákos csomagolásra, 10-50 kg-os méréshatárral.

A projekt első fázisában a tervezés valósul meg, mely alapján a második fázisban elkészül a prototípus. A harmadik fázisban a megépítendő próbaállomás segítségével történik a berendezés tesztelése és a tapasztalatok alapján a visszacsatolás, a tervek és a prototípus korrigálása, melynek eredményeként elkészül a piacképes termék. A piacra történő bevezetés céljából különböző vásárokon, kiállításokon mutatjuk be az új mérlegünket.

A fejlesztés a Gazdaságfejlesztési és Innovációs Operatív Program keretében, az Európai Regionális Fejlesztési Alap társfinanszírozásával valósult meg.