#ifndef SHAREDVARS_H
#define SHAREDVARS_H

#include <QObject>
#include "parameters.h"

class SharedVars : public QObject
{
    Q_OBJECT
public:
    SharedVars(const QString TaroloFile, ParMap* DefaultParMap);
    virtual void Edit(std::function<void(ParMap *)> editrutin);
    ParMap GetValue(){QMutexLocker locker(&SMutex); return V;}
signals:
    void modified();
protected:
    QMutex SMutex;
    ParMap V;
    QString FileName;
    QString HibaUtasitas;
    bool mergeMustSaveS;
    void Merge(ParMap *S);
    virtual bool LoadStatic(ParMap *S);
    bool LoadSfromFile(ParMap *S,const QString File);
    virtual void SaveStatic();
    void SaveVtoFile(const QString File);
    void saveHash (const QString &fileName);
    void InvalidFileDialog();
    QString loadSavedHash(const QString &fileName);
    QByteArray fileChecksum(const QString &fileName);
    QString myConvertedHash(const QString &fileName);
};


#endif // SHAREDVARS_H
