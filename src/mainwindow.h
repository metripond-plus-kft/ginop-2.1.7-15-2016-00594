#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "s7plc.h"
#include "plcmenudialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void refreshTechDisplay();
    void hibaUzenet(QString HibaUzenet);
    void atmenetiUzenet(QString Uzenet, int IdotartamMs);
    void on_menuToolButton_clicked();
    void on_stopToolButton_clicked();
    void on_folytatToolButton_clicked();
    void on_zsakleToolButton_clicked();
    void on_clearErrorToolButton_clicked();
    void on_muszakTorlesToolButton_clicked();

    void on_autoCheckBox_toggled(bool checked);

private:
    Ui::MainWindow *ui;
    S7plc Plc;
    plcMenuDialog *m_menuDialog;
    QTimer atmenetiTimer;
};

#endif // MAINWINDOW_H
