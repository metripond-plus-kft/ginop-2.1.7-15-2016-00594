#ifndef S7PLC_H
#define S7PLC_H

#include "snap7.h"
#include "plcvars.h"
#include "scaledisplay1.h"

enum PlcComState {ERROR,NOCONNECTION,INITIALIZATION,ADATCSERE};
enum PlcParancs:unsigned char{NINCS=0,STOP=1,FOLYTAT=2,ZSAKLE=4};
enum ScaleFlags:unsigned char{NYUGALOM=1,FOLYNYUG=2,FNULLA=4,ALULT=8,TULT=16,AUTO=32};

class S7plc: public QObject
{
    Q_OBJECT
public:
    S7plc(QObject *parent = 0);
    ~S7plc();
    LocalParVars* getPp(){return &PlcPars;}
    TechVars* getTp(){return &TechPars;}
    PlcMemoryVars* getStoPlcv(){return &ScaleToPlcVars;}
    PlcMemoryVars* getSpsv(){return &SetPlcStoredVars;}
    PlcMemoryVars* getGpsv(){return &GetPlcStoredVars;}
    PlcMemoryVars* getPsv(){return &PlcStateVars;}
    ScaleDisplay1* getScaleDisplayptr(){return m_scaledisplayptr;}
    void start(ScaleDisplay1 *scaledisplay);
signals:
    void sendAtmenetiUzenet(QString Uzenet, int Idotartam_ms);
    void sendHibaUzenet(QString Uzenet);
    void newPlcdata();

public slots:
    void clearError();
    void techParsModified(){m_newtechvars=true;}
    void plcStoredVarsModified(){m_newsetplcstoredpars=true;}
    void sendPlcParancs(const unsigned char parancs){m_parancs=parancs;}
    void clearMuszak();
    void setAuto(const bool automode){m_auto=automode;}

private slots:
    void s7ComProcess();
    void ms250Process();
    void secProcess();

private:
    TS7Client S7Client;
    ScaleDisplay1 *m_scaledisplayptr;
    LocalParVars PlcPars;     //PLC kapcsolati parameterek
    TechVars TechPars;      //Muszer menuben szerkesztett mukodesi parameterek
    PlcMemoryVars ScaleToPlcVars; //Muszer altal folyamatosan kuldott adatok
    PlcMemoryVars SetPlcStoredVars; //PLC memoria valtozok modositasahoz
    PlcMemoryVars GetPlcStoredVars; //PLC memoria valtozok kiolvasashoz
    PlcMemoryVars PlcStateVars;     //PLC-bol folyamatosan beolvasott adatok
    QTimer* timer_s7;
    QTimer* timer_sec;
    QTimer* timer_250ms;
    PlcComState ComState;
    QString Hibauzenet;
    bool m_newsetplcstoredpars;
    bool m_newtechvars;
    float m_tomeg;
//    ScaleFlags m_scaleflags;
    unsigned char m_scaleflags;
    unsigned char m_commatempt;
    uchar m_parancs;
    bool m_readplcdata;
    bool m_readplcstoredvars;
    bool m_auto;
    bool connectToPlcc();
    bool writeTechParsToPlc();
    bool writeSetPlcStoredVarsToPlc();
    bool writeScaletoPlcVarsToPlc();
    bool readPlcStateVarsFromPlc();
    bool readGetPlcStoredVarsFromPlc();
    void setComOk(); //Hibauzenet torles
};

#endif // S7PLC_H
