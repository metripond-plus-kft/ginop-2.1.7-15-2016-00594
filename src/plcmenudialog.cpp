#include "plcmenudialog.h"
#include "ui_plcmenudialog.h"
#include <QListView>

plcMenuDialog::plcMenuDialog(QWidget *parent, S7plc *Plcptr) :
    QDialog(parent),
    ui(new Ui::plcMenuDialog),
    m_plcptr(Plcptr)
{
    ui->setupUi(this);
    this->setWindowTitle(QObject::tr("Setup"));

    ui->fomenuComboBox->setView(new QListView());
    ui->fomenuComboBox->setStyleSheet("QComboBox QAbstractItemView::item { min-height: 50px; min-width: 50px; }");
    ui->editPushButton->setText(QObject::tr("Módosít"));
    ui->closePushButton->setText(QObject::tr("Bezár"));

    ParMap gpsv=Plcptr->getGpsv()->GetValue(); //Plc aktualis ertekek
    Plcptr->getSpsv()->Edit([&] (ParMap *pm)   //atmasolasa a letoltesi ertekekbe
    {(*pm)["4_Utanhullas"]=gpsv["4_Utanhullas"];
     (*pm)["4_Muszakdb"]=gpsv["4_Muszakdb"];
     (*pm)["4_Muszaksum"]=gpsv["4_Muszaksum"];
     (*pm)["4_Db"]=gpsv["4_Db"];
     (*pm)["4_Sum"]=gpsv["4_Sum"];
     (*pm)["4_Tarasuly"]=gpsv["4_Tarasuly"];
    });
    std::unique_ptr<PlcMenuTableModel> tmpPtr(new PlcMenuTableModel(0,Plcptr));
    m_modelPtr=std::move(tmpPtr);
    ui->fomenuComboBox->insertItems(0,m_modelPtr.get()->getFomenuNames());
}

plcMenuDialog::~plcMenuDialog()
{
    delete ui;
    if (m_editDialog)
        delete m_editDialog;
    //m_editDialog QPointer
    //torles utan automatikusan nullptr az ertekuk
    //elvileg az if() sem kene, mert delete nullptr is biztonsagos
}

void plcMenuDialog::on_fomenuComboBox_currentIndexChanged(int index)
{
    std::unique_ptr<PlcMenuTableModel> tmpPtr(new PlcMenuTableModel(index,m_plcptr));
    ui->menuTableView->setModel(tmpPtr.get());
    ui->menuTableView->resizeColumnsToContents();

    m_modelPtr=std::move(tmpPtr);
}

void plcMenuDialog::on_editPushButton_clicked()
{
  QModelIndex index = ui->menuTableView->currentIndex();
  if (!index.isValid())
   return;
  m_item =m_modelPtr.get()->getMenuItem(index);
  switch (m_item.Editor) {
  case EDITDIALOG:{
      if (m_editDialog)
       delete m_editDialog;
      m_editDialog=new PlcEditDialog(m_item,&m_ujertek,this);
      connect(m_editDialog,SIGNAL(accepted()),this,SLOT(itemModified()));
      connect(m_editDialog,SIGNAL(rejected()),this,SLOT(itemNotModified()));
//      m_editDialog->setAttribute(Qt::WA_DeleteOnClose);
// accep() es reject() csak hide()-ot hiv, nem close()-t vagy done()-t, aminel a WA_DeleteOnClose hatasos
      m_editDialog->setModal(true);
      m_editDialog->show();
      break;}
  default:
      break;
  };

}

void plcMenuDialog::itemModified()
{
    switch (m_item.Editor) {
    case EDITDIALOG:{
      m_item.Tarolo->Edit([&] (ParMap *pm)
      {(*pm)[m_item.ValtozoId]=m_ujertek;});
      ui->menuTableView->resizeColumnsToContents();
        break;}
    default:
        break;
    };
    if (m_item.Tarolo==m_plcptr->getSpsv())
     m_plcptr->plcStoredVarsModified();
    if (m_item.Tarolo==m_plcptr->getTp())
     m_plcptr->techParsModified();
}

void plcMenuDialog::itemNotModified()
{
    switch (m_item.Editor) {
    case EDITDIALOG:
        break;
    default:
        break;
    };
}

