#ifndef PLCMENUDIALOG_H
#define PLCMENUDIALOG_H

#include <memory>
#include <QDialog>
#include "plcparmenu.h"
#include "plceditdialog.h"

namespace Ui {
class plcMenuDialog;
}

class plcMenuDialog : public QDialog
{
    Q_OBJECT

public:
    explicit plcMenuDialog(QWidget *parent, S7plc *Plcptr);
    ~plcMenuDialog();

private slots:
    void on_fomenuComboBox_currentIndexChanged(int index);
    void on_editPushButton_clicked();
    void itemModified();
    void itemNotModified();

private:
    Ui::plcMenuDialog *ui;
    std::unique_ptr<PlcMenuTableModel> m_modelPtr;
    QPointer<PlcEditDialog> m_editDialog;
    PlcMenuItem m_item;
    QVariant m_ujertek;
    S7plc *m_plcptr;
};

#endif // PLCMENUDIALOG_H
