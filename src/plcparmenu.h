#ifndef PLCPARMENU_H
#define PLCPARMENU_H
#include <QMap>
#include <QObject>
#include <QAbstractTableModel>
#include <limits>
#include "s7plc.h"
#define MAXTOMEG    std::numeric_limits<double>::max()
#define MAXUINT    std::numeric_limits<unsigned int>::max()

//enum PlcEditRutin {NO,EDITDIALOG};

class PlcMenuTableModel;//forward declaration
typedef struct plcmenu_item
{
    PlcMenuTableModel *Owner;
    QString NevStr;
    SharedVars *Tarolo;
    QByteArray ValtozoId;
    QByteArray UnitId;
    int Tizedes;
    double Min;
    double Max;
    ParMap *Felsorolas;
    EditRutin Editor;
    QString UnitStr() const;
    double   getMax() const;
    int   getDecimal() const;
    bool isIntType(QMetaType::Type vtype) const;
    QString VariantToString(const QVariant variant, const int displayedDecimal) const;
    QStringList getFelsorolasDisplayValues(ParMap *Felsorolas, const int displayedDecimal) const;
    QStringList getFelsorolasStoredValues(ParMap *Felsorolas) const;
}PlcMenuItem;

typedef struct plcfomenu_item
{
    QString NevStr;
    QMap<int,PlcMenuItem> *MenuMapPtr;
    int Count;
}PlcFomenuItem;


class PlcMenuTableModel : public QAbstractTableModel
{
      public:
        PlcMenuTableModel(const int fomenuItem, S7plc *Plcptr);
        int rowCount(const QModelIndex& parent) const;
        int columnCount(const QModelIndex& parent) const;
        QVariant data(const QModelIndex& index, int role) const;
        QVariant headerData(int section, Qt::Orientation orientation, int role) const;
        Qt::ItemFlags flags(const QModelIndex &index) const;
        bool setData(const QModelIndex &index, const QVariant &value, int role=Qt::EditRole);
        QStringList getFomenuNames();
        int getFomenuItemId(){return m_FomenuItemId;}
        PlcMenuItem getMenuItem(const QModelIndex& index);
        ParMap* getCalpmptr(){return &m_calpm;}
        static ParMap UnitMap;
        static ParMap IgenNemList;
       protected:
        QMap<int,PlcMenuItem> *m_menuMapPtr;
        int m_count;
        int m_FomenuItemId;
        ParMap m_calpm;
        SharedVars* m_Pp;
        TechVars* m_Tp;
        PlcMemoryVars* m_Spsv;
        PlcMemoryVars* m_Psv;
        QMap<int,PlcFomenuItem> m_foMenu;
        QMap<int,PlcMenuItem> PlcMenu;
        QMap<int,PlcMenuItem> TechMenu;
        QMap<int,PlcMenuItem> AdagMenu;
        void selectMenu(const int menuId);
        void inicPlcMenu();
        void inicTechMenu();
        void inicAdagMenu();
        void inicFoMenu();

};


#endif // PLCPARMENU_H
