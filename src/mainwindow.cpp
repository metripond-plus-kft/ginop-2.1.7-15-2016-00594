#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    Plc(parent)
{
    ui->setupUi(this);
//teszt
// Plc.getTp()->writePlcdataToV();
// Plc.getSpsv()->writePlcdataToV();
//teszt veg
    QString prgver=ui->prgVersionlabel->text();
    prgver.replace("xxxxx",ui->scaleDisplay1->getShortVersion());
    ui->prgVersionlabel->setText(prgver);
    ui->scaleDisplay1->start(0);
    Plc.start(ui->scaleDisplay1);
    connect(&Plc, SIGNAL(sendHibaUzenet(QString)),
            this, SLOT(hibaUzenet(QString)));
    connect(&Plc, SIGNAL(newPlcdata()),
            this, SLOT(refreshTechDisplay()));
    connect(&Plc, SIGNAL(sendAtmenetiUzenet(QString,int)), this, SLOT(atmenetiUzenet(QString,int)));

#ifdef QT_DEBUG
    ui->debugLabel->setVisible(true);
#else
    ui->debugLabel->setVisible(false);
#endif
    ui->atmenetiUzenetLabel->setText("");
    ui->hibaLabel->setText("");
    ui->clearErrorToolButton->setEnabled(false);
    Plc.setAuto(ui->autoCheckBox->isChecked());

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_menuToolButton_clicked()
{
    m_menuDialog=new plcMenuDialog(this,&Plc);
    m_menuDialog->setAttribute(Qt::WA_DeleteOnClose);
    m_menuDialog->setModal(true);
    m_menuDialog->show();
}


void MainWindow::refreshTechDisplay()
{
   ParMap plcstate=Plc.getPsv()->GetValue();
   ParMap storedvars=Plc.getGpsv()->GetValue();
   TomegData td=ui->scaleDisplay1->getTomegData();
   ParMap cv= ui->scaleDisplay1->getCalibrationMap();
   quint32 Muszakdb=storedvars["4_Muszakdb"].toUInt();
   double Muszaksum=LepesreKerekit(storedvars["4_Muszaksum"].toDouble(),td.s_AktLepes);
   quint32 Db=storedvars["4_Db"].toUInt();
   double Sum=LepesreKerekit(storedvars["4_Sum"].toDouble(),td.s_AktLepes);

   ScaleFlags Scaleflags=(ScaleFlags)plcstate["1_Scaleflags"].toUInt();
   double Toltet=LepesreKerekit(plcstate["4_Toltettomeg"].toDouble(),td.s_AktLepes);
   uchar Prgall=(uchar)plcstate["1_Prgall"].toUInt();
   uchar Plcflags=(uchar)plcstate["1_Plcflags"].toUInt();
   uchar In=(uchar)plcstate["1_In"].toUInt();
   uchar Out=(uchar)plcstate["1_Out"].toUInt();
   double TzNyitkesl=plcstate["1_Tznyitkesl"].toDouble()/10;

   QString tomegstr;
   if (Scaleflags&ALULT || Scaleflags&TULT)
       tomegstr="- - - - - -";
   else
       tomegstr=QString::number(Toltet,'f',td.s_Tomegtizedes);
   tomegstr += " ";
   tomegstr += cv["Mertekegyseg"].toString();
   ui->toltetLabel->setText(tomegstr);
   ui->muszakDbLabel->setText(QString::number(Muszakdb));
   tomegstr=QString::number(Muszaksum,'f',td.s_Tomegtizedes);
   tomegstr += " ";
   tomegstr += cv["Mertekegyseg"].toString();
   ui->muszakSumLabel->setText(tomegstr);
   ui->dbLabel->setText(QString::number(Db));
   tomegstr=QString::number(Sum,'f',td.s_Tomegtizedes);
   tomegstr += " ";
   tomegstr += cv["Mertekegyseg"].toString();
   ui->sumLabel->setText(tomegstr);
   ui->tzNyitLabel->setVisible((TzNyitkesl>0) && ((In & 128)>0));
   ui->tzNyitLabel->setText(tr("Tólózár nyit: %1s").arg(QString::number(TzNyitkesl,'f',1)));

   if ((Plcflags & 4)>0)
       atmenetiUzenet(tr("Nincs elég anyag"),1000);

   ui->ledIn0->setState((In & 1)>0);
   ui->ledIn1->setState((In & 2)>0);
   ui->ledIn2->setState((In & 4)>0);
   ui->ledIn3->setState((In & 8)>0);
   ui->ledIn4->setState((In & 16)>0);
   ui->ledIn5->setState((In & 32)>0);
   ui->ledIn6->setState((In & 64)>0);
   ui->ledIn7->setState((In & 128)>0);

   ui->ledOut0->setState((Out & 1)>0);
   ui->ledOut1->setState((Out & 2)>0);
   ui->ledOut2->setState((Out & 4)>0);
   ui->ledOut3->setState((Out & 8)>0);
   ui->ledOut4->setState((Out & 16)>0);
   ui->ledOut5->setState((Out & 32)>0);
//   ui->ledOut6->setState((Out & 64)>0);
   ui->ledOut6->setState(ui->ledOut1->state()); //reteg szab a durva adagolassal egyutt nyit
   ui->ledOut7->setState((Out & 128)>0);
//   ui->ledOut6->setVisible(false); //csak 5 kimenet van
   ui->ledOut7->setVisible(false); //csak 5 kimenet van

   ui->stopToolButton->setEnabled(Prgall!=0);
   ui->zsakleToolButton->setEnabled((Out & 16)>0);
   ui->folytatToolButton->setEnabled((Prgall==0) && ((Out & 16)>0));
   if (Prgall==0)
   {
     ui->allapotLabel->setText(tr("STOP"));
     ui->hibaLabel->setText("");
     ui->clearErrorToolButton->setEnabled(false);
   }
   else if (Prgall<128)
   {//adagolas
     ui->allapotLabel->setText(tr("ADAGOL(%1)").arg(Prgall));

   }
   else
   { //hiba allapotok
     ui->allapotLabel->setText(tr("HIBA(%1)").arg(Prgall));
       switch (Prgall) {
       case 128:
            hibaUzenet(tr("PLC Profinet hiba!"));
           break;
       case 129:
           hibaUzenet(tr("Ívzár hiba!"));
           break;
       case 130:
           hibaUzenet(tr("Tolózár hiba!"));
           break;
       case 131:
           hibaUzenet(tr("Utánhullás túl nagy!"));
           break;
       case 132:
           hibaUzenet(tr("Nem tárázható - Bruttó > Tára határ!"));
           break;
       case 133:
           hibaUzenet(tr("Táplevegő nyomás elégtelen!"));
           break;
       case 200:
           hibaUzenet(tr("Ismeretlen PLC program állapot!"));
           break;
       default:
           hibaUzenet(tr("Ismeretlen hiba: %1").arg(Prgall));
           break;
       }
   }


}

void MainWindow::on_stopToolButton_clicked()
{
    Plc.sendPlcParancs(STOP);
}

void MainWindow::on_folytatToolButton_clicked()
{
    Plc.sendPlcParancs(FOLYTAT);
}

void MainWindow::on_zsakleToolButton_clicked()
{
    Plc.sendPlcParancs(ZSAKLE);
}

void MainWindow::on_muszakTorlesToolButton_clicked()
{
    Plc.clearMuszak();
}

void MainWindow::on_clearErrorToolButton_clicked()
{
    Plc.clearError();
}

void MainWindow::hibaUzenet(QString HibaUzenet)
{
   if (!ui->clearErrorToolButton->isEnabled())
   {
       ui->hibaLabel->setText(HibaUzenet);
       ui->clearErrorToolButton->setEnabled(true);
   }
}

void MainWindow::atmenetiUzenet(QString Uzenet, int IdotartamMs)
{
    if (ui->atmenetiUzenetLabel->text()=="")
    {
        ui->atmenetiUzenetLabel->setText(Uzenet);
        atmenetiTimer.singleShot(IdotartamMs,this,[this]{ui->atmenetiUzenetLabel->setText("");});
    }
}

void MainWindow::on_autoCheckBox_toggled(bool checked)
{
    Plc.setAuto(checked);
}
