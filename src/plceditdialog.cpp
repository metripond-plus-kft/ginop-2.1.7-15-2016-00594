#include "plceditdialog.h"
#include "ui_plceditdialog.h"
#include <QMessageBox>
#include <QtMath>

PlcEditDialog::PlcEditDialog(const PlcMenuItem &editItem, QVariant* result_variant, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PlcEditDialog),
    m_menuItem(editItem),    //deep copy - szandekosan, nem gond, ha edititem-et felszabaditjak
    m_result_variant(result_variant)
{
    ui->setupUi(this);
    this->setWindowTitle(m_menuItem.NevStr);
    ui->parNameLabel->setText(m_menuItem.NevStr);
    if (m_menuItem.Min==0 && m_menuItem.Max==0)
    {
      ui->minLabel->setText("-");
      ui->maxLabel->setText("-");
    }
    else
    {
     ui->minLabel->setText(QString::number(m_menuItem.Min));
     ui->maxLabel->setText(QString::number(m_menuItem.getMax()));
    };
    ui->mertekEgysegLabel->setText(m_menuItem.UnitStr());
    ParMap pm(m_menuItem.Tarolo->GetValue());
    m_variant=pm[m_menuItem.ValtozoId];
    QString valueStr=m_menuItem.VariantToString(m_variant,m_menuItem.getDecimal());

    if (m_menuItem.Felsorolas==nullptr)
    {
      m_editor=ui->lineEdit;
      ui->lineEdit->setText(valueStr);
      ui->comboBox->setVisible(false);
      QMetaType::Type vartype=static_cast<QMetaType::Type>(m_variant.type());
      if (vartype==QMetaType::QString)
         m_keyBoard= new widgetKeyBoard(true,this); //kis méret
      else
         m_keyBoard= new numericWidgetKeyboard(false,this); //nagy méret
      m_keyBoard->setZoomFacility(true);
      m_keyBoard->enableSwitchingEcho(true); // enable possibility to change echo through keyboard
      m_keyBoard->createKeyboard(); // only create keyboard
      m_keyBoard->focusThis(ui->lineEdit);
      ui->verticalLayout->addWidget(m_keyBoard);
    }
    else
    {
      m_editor=ui->comboBox;
      ui->lineEdit->setVisible(false);
      ui->comboBox->setGeometry(ui->lineEdit->geometry());
      m_felsorolasStoredValues=m_menuItem.getFelsorolasStoredValues(m_menuItem.Felsorolas);
      ui->comboBox->insertItems(0,m_menuItem.getFelsorolasDisplayValues(m_menuItem.Felsorolas,m_menuItem.getDecimal()));
      int index = ui->comboBox->findText(valueStr);
      if ( index != -1 ) { // -1 for not found
         ui->comboBox->setCurrentIndex(index);
      };
    }
}

PlcEditDialog::~PlcEditDialog()
{
    delete ui;
}

void PlcEditDialog::accept()
{
    bool kilephet=false;
    QVariant test;
    if (m_editor==ui->lineEdit)
    {
        test.setValue(ui->lineEdit->text());
    }
    else if (m_editor==ui->comboBox)
    {

        test.setValue(m_felsorolasStoredValues.at(ui->comboBox->currentIndex()));
    }
    else return;

    QMetaType::Type vartype=static_cast<QMetaType::Type>(m_variant.type());
    if (vartype==QMetaType::QString)
    {
        m_result_variant->setValue(test.toString());
        kilephet=true;
    }
    else
    {
      QString testValue=test.toString();
      if (vartype!=QMetaType::Bool)
       if (test.convert(QMetaType::Double))
       {
        if (m_menuItem.Min!=0 || m_menuItem.Max!=0)
        {
         double testit=test.toDouble();
         if ((testit<m_menuItem.Min) || (testit>m_menuItem.getMax()))
          {
             QMessageBox::warning(this,QObject::tr("Hibás érték!"),QObject::tr("Értéknek min. és max. közé kell esnie!"));
             return;
          };
         };
         if (m_menuItem.isIntType(vartype))
         {
             test= test.toDouble()*qPow(10,m_menuItem.getDecimal());
             testValue= QString::number(test.toDouble(),'f',0);
         }
        };
      test.setValue(testValue);
      if (test.convert(vartype))
      {
       m_result_variant->setValue(testValue);
       m_result_variant->convert(vartype);
       kilephet=true;
      }
      else
       QMessageBox::warning(this,QObject::tr("Konvertálási hiba!"),QObject::tr("Hibás adat formátum!"));
    }

    if (kilephet)
        QDialog::accept();

}

