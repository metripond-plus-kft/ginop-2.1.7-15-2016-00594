#include "plcvars.h"

//int mygetPlcLength(const QMetaType::Type vtype)
//{
//    switch (vtype)
//    {
//     case QMetaType::Bool:    //PLC-ben egy byte
//     case QMetaType::Char:
//     case QMetaType::UChar:
//     case QMetaType::SChar:
//            return 1;
//          break;
//    case QMetaType::QChar:
//    case QMetaType::Short:
//    case QMetaType::UShort:
//        return 2;
//      break;
//    case QMetaType::Int:
//    case QMetaType::UInt:
//    case QMetaType::Float:
//    case QMetaType::Double:     //PLC-ben nincs 64bit float
//        return 4;
//      break;
//    default:
//        return 4;
//      break;
//    }
//}

//int writeVariantToQByteArray(QVariant *Var,QByteArray *Qba, const int pos)
//{
//    QMetaType::Type vtype=static_cast<QMetaType::Type>(Var->type());
//    QVariant tmp;
//    if (vtype==QMetaType::Double)
//    {
//        float ftmp = Var->toFloat();
//        tmp=ftmp;
//    }
//    else if (vtype==QMetaType::Bool)
//    {
//        if (Var->toBool())
//         tmp=(char)1;
//        else
//         tmp=(char)0;
//    }
//    else
//        tmp=(*Var);

//    int length=mygetPlcLength(vtype);
//    QByteArray buffer=tmp.toByteArray();
//    for (char i=0;i<length;i++)
//      (*Qba)[pos+i]=buffer[length-i-1];
//    return (pos+length);
//}

//int writeQByteArrayToVariant(QVariant *Var,QByteArray *Qba, const int pos)
//{
//    QMetaType::Type vtype=static_cast<QMetaType::Type>(Var->type());
//    int length=mygetPlcLength(vtype);
//    QByteArray buffer;
//    buffer.resize(length);
//    for (char i=0;i<length;i++)
//      buffer[length-i-1]=(*Qba)[pos+i];
//    if (vtype==QMetaType::Float)
//    {
//        float ftmp = *reinterpret_cast<const float*>(buffer.data());
//        *Var=ftmp;
//    }
//    if (vtype==QMetaType::Double)
//    {
//        float ftmp = *reinterpret_cast<const float*>(buffer.data());
//        *Var=(double)ftmp;
//    }
//    else if (vtype==QMetaType::Bool)
//    {
//        bool b=char(buffer[0])!=0;
//        Var->setValue(b);
//    }
//    else if (vtype==QMetaType::Short)
//    {
//        qint16 itmp = *reinterpret_cast<const qint16*>(buffer.data());
//        Var->setValue(itmp);
//    }
//    else if (vtype==QMetaType::UShort)
//    {
//        quint16 uitmp = *reinterpret_cast<const quint16*>(buffer.data());
//        Var->setValue(uitmp);
//    }
//    else if (vtype==QMetaType::Char || vtype==QMetaType::SChar)
//    {
//        qint8 itmp = *reinterpret_cast<const qint8*>(buffer.data());
//        Var->setValue(itmp);
//    }
//    else if (vtype==QMetaType::UChar)
//    {
//        quint8 uitmp = *reinterpret_cast<const quint8*>(buffer.data());
//        Var->setValue(uitmp);
//    }
//    else // Int, UInt, QChar
//    {
//     Var->setValue(buffer);
//     Var->convert(vtype);
//    };
//    return (pos+length);
//}

int mygetPlcLength(const QByteArray &key)
{
   char chr=key[0];
   return chr-48;   //48: '0'
}

int writeVariantToQByteArray(const QByteArray &key,QVariant *Var,QByteArray *Qba, const int pos)
{
    int length=mygetPlcLength(key);
    QByteArray buffer;
    QMetaType::Type vtype=static_cast<QMetaType::Type>(Var->type());
    if ((vtype==QMetaType::Double) || (vtype==QMetaType::Float))
    {
        float ftmp = Var->toFloat();
        buffer=QByteArray(reinterpret_cast<const char*>(&ftmp), length);
    }
    else if (vtype==QMetaType::Bool)
    {
        char ctmp;
        if (Var->toBool())
         ctmp=(char)1;
        else
         ctmp=(char)0;
        buffer=QByteArray(reinterpret_cast<const char*>(&ctmp), length);
    }
    else
    {
        int itmp=Var->toInt();
        buffer=QByteArray(reinterpret_cast<const char*>(&itmp), length);
    }

//    QByteArray buffer=tmp.toByteArray();
//    QByteArray buffer=(reinterpret_cast<const char*>(&tmp.value), length);
    for (char i=0;i<length;i++)
      (*Qba)[pos+i]=buffer[length-i-1];
    return (pos+length);
}

int writeQByteArrayToVariant(const QByteArray &key,QVariant *Var,QByteArray *Qba, const int pos)
{
    QMetaType::Type vtype=static_cast<QMetaType::Type>(Var->type());
    int length=mygetPlcLength(key);
    QByteArray buffer;
    buffer.resize(length);
    for (char i=0;i<length;i++)
      buffer[length-i-1]=(*Qba)[pos+i];
    if (vtype==QMetaType::Float)
    {
        float ftmp = *reinterpret_cast<const float*>(buffer.data());
        *Var=ftmp;
    }
    else if (vtype==QMetaType::Double)
    {
        float ftmp = *reinterpret_cast<const float*>(buffer.data());
        *Var=(double)ftmp;
    }
    else if (vtype==QMetaType::Bool)
    {
        bool b=char(buffer[0])!=0;
        *Var=b;
    }
    else // Int   - char, uchar, sint, suint int-kent tarolodik
    {
      if (length==4)
      {
        int itmp = *reinterpret_cast<const int*>(buffer.data());
        *Var=itmp;
      }
      else //length==1
      {
        uchar uctmp = *reinterpret_cast<const unsigned char*>(buffer.data());
        *Var=uctmp;
      }
    }
    return (pos+length);
}

LocalParVars::LocalParVars(const QString TaroloFile, ParMap* DefaultParMap):
    SharedVars(TaroloFile,DefaultParMap)
{
    Initialize();
}

void LocalParVars::Initialize()
{
    // csak lockolt helyrol v. constructorbol hivhato!
    //V=default ParMap value
    HibaUtasitas = tr("Ellenőrizze a technológiai menü beállításait!");
    mergeMustSaveS=false;
    ParMap pm;
    if (LoadStatic(&pm))
     Merge(&pm);
    else
     SaveStatic(); //default parmap elmentese
}


TechVars::TechVars(const QString TaroloFile, ParMap* DefaultParMap, QString *order):
    SharedVars(TaroloFile,DefaultParMap),
    orderarray(order)
   {
    Initialize();
   }

void TechVars::Initialize()
{
    // csak lockolt helyrol v. constructorbol hivhato!
    //V=default ParMap value
    if (FileName.indexOf(".")>0)
    {
     HibaUtasitas = tr("Ellenőrizze a technológiai paramétereket!");
     mergeMustSaveS=false;
     ParMap pm;
     if (LoadStatic(&pm))
      Merge(&pm);
     else
      SaveStatic(); //default parmap elmentese
    }
    PlcDataLength=getVDataLength();
    PlcData.fill(0,PlcDataLength);
    writeVtoPlcdata();
}


int TechVars::getVDataLength()
{
    int result=0;
    foreach (const QByteArray &key, V.keys())
    {
//       QMetaType::Type t=static_cast<QMetaType::Type>(V[key].type());
//       int i=mygetPlcLength(t);
       int i=mygetPlcLength(key);
       result+=i;
    }
    return result;
}

void TechVars::writeVtoPlcdata()
{
    int position=0;
//  igy key szerint rendezve jarja be, nem a letrehozas sorrendjeben
//    foreach (const QByteArray &key, V.keys())
//    {
//      position=writeVariantToQByteArray(key,&V[key],&PlcData,position);
//    }
    QByteArray key;
    int i=0;
    while (*(orderarray+i)!="end")
    {
      key=(orderarray+i)->toUtf8();
      position=writeVariantToQByteArray(key,&V[key],&PlcData,position);
      i++;
    }
}

void TechVars::writePlcdataToV()
{
    QMutexLocker locker(&SMutex);
    int position=0;
//  igy key szerint rendezve jarja be, nem a letrehozas sorrendjeben
//    foreach (const QByteArray &key, V.keys())
//    {
//      position=writeQByteArrayToVariant(key,&V[key],&PlcData,position);
//    }
    QByteArray key;
    int i=0;
    while (*(orderarray+i)!="end")
    {
      key=(orderarray+i)->toUtf8();
      position=writeQByteArrayToVariant(key,&V[key],&PlcData,position);
      i++;
    }

}

void TechVars::Edit(std::function<void(ParMap *)> editrutin)
{
// folyamatos lockolas kell, hogy kozben ne lehessen modositas
    {
    QMutexLocker locker(&SMutex);
    editrutin(&V);
    SaveStatic();
    writeVtoPlcdata();
    }
    emit modified();
}


PlcMemoryVars::PlcMemoryVars(const QString TaroloFile, ParMap* DefaultParMap, QString *order):
    TechVars(TaroloFile,DefaultParMap,order)
   {
    HibaUtasitas = tr("Ellenőrizze a PLC kommunikációt!");
   }

//void PlcMemoryVars::Initialize()
//{
//    // csak lockolt helyrol v. constructorbol hivhato!
//    //V=default ParMap value
//    HibaUtasitas = tr("Ellenőrizze a PLC kommunikációt!");
//    PlcDataLength=getVDataLength();
//    PlcData.fill(0,PlcDataLength);
//    writeVtoPlcdata();
//}
