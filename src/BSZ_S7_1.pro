#-------------------------------------------------
#
# Project created by QtCreator 2019-11-20T19:07:10
#
#-------------------------------------------------

QT       += core gui
QT       += serialport
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BSZ_S7_1
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    ../../MyPlugins/led-designer-plugin/LED.cpp \
    ../../Snap7_S7PLC_kommunikacio/snap7.cpp \
    s7plc.cpp \
    sharedvars.cpp \
    plcvars.cpp \
    plcparmenu.cpp \
    ../../MyPlugins/myVirtualKeyboard/numericwidgetkeyboard.cpp \
    ../../MyPlugins/myVirtualKeyboard/QKeyPushButton.cpp \
    ../../MyPlugins/myVirtualKeyboard/widgetKeyBoard.cpp \
    plcmenudialog.cpp \
    plceditdialog.cpp

HEADERS += \
        mainwindow.h \
    ../../MyPlugins/led-designer-plugin/LED.h \
    ../../Snap7_S7PLC_kommunikacio/snap7.h \
    s7plc.h \
    sharedvars.h \
    plcvars.h \
    plcparmenu.h \
    ../../MyPlugins/myVirtualKeyboard/numericwidgetkeyboard.h \
    ../../MyPlugins/myVirtualKeyboard/QKeyPushButton.h \
    ../../MyPlugins/myVirtualKeyboard/widgetKeyBoard.h \
    plcmenudialog.h \
    plceditdialog.h

FORMS += \
        mainwindow.ui \
    plcmenudialog.ui \
    plceditdialog.ui

INCLUDEPATH += ../../SharedObjects/Common
INCLUDEPATH += ../../Snap7_S7PLC_kommunikacio
INCLUDEPATH += ../../MyPlugins/myVirtualKeyboard
INCLUDEPATH += ../../MyPlugins/led-designer-plugin

#SDVERSION=1.2
SDVERSION=1.3
equals(SDVERSION,1.2)
{
INCLUDEPATH += ../../SharedObjects/ScaleDisplay1_2tomegkalkulatorral/ScaleDisplay1_2/
DEPENDPATH += ../../SharedObjects/ScaleDisplay1_2tomegkalkulatorral/ScaleDisplay1_2/

LIBS += -L/home/bzoli/LocalShareObjects/ScaleDisplay1_2tomegkalkulatorral/build-ScaleDisplay1-Clone_of_Raspberry_Pi3-Debug/ -lScaleDisplay1
}

equals(SDVERSION,1.3)
{
INCLUDEPATH += ../../SharedObjects/ScaleDisplay1_3crc32/ScaleDisplay1_3/
DEPENDPATH += ../../SharedObjects/ScaleDisplay1_3crc32/ScaleDisplay1_3/

LIBS += -L/home/bzoli/LocalShareObjects/ScaleDisplay1_3crc32/build-ScaleDisplay1-Clone_of_Raspberry_Pi3-Debug/ -lScaleDisplay1
}


LIBS += -lwiringPi
LIBS += -lsnap7

target.path = /home/pi/Desktop
INSTALLS += target

TRANSLATIONS += ../translations/BSZ_S7_1_hu.ts
TRANSLATIONS += ../translations/BSZ_S7_1_xx.ts

RESOURCES += \
    resources.qrc \
    ../../MyPlugins/myVirtualKeyboard/virtualboard.qrc
