#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>
#include "scaledisplay1.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setApplicationName("BSZ_S7_1");
    a.setOrganizationName("MetripondPlus");
    a.setOrganizationDomain("mplus.hu");

    QString libQmFile = QApplication::applicationDirPath();
    libQmFile += "/ScaleDisplay1_xx";
    QTranslator libTranslator;
    if (ScaleDisplay1::loadTranslation(libTranslator, libQmFile))
     a.installTranslator(&libTranslator);

    QString appQmFile = QApplication::applicationDirPath();
    appQmFile += "/BSZ_S7_1_xx";
    QTranslator appTranslator;
    if (appTranslator.load(appQmFile))
     a.installTranslator(&appTranslator);


    MainWindow w;
    w.setWindowFlags(Qt::FramelessWindowHint);
    w.show();

    return a.exec();
}
