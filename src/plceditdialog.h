#ifndef PLCEDITDIALOG_H
#define PLCEDITDIALOG_H

#include <QDialog>
#include "plcparmenu.h"
#include "widgetKeyBoard.h"
#include "numericwidgetkeyboard.h"

namespace Ui {
class PlcEditDialog;
}

class PlcEditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PlcEditDialog(const PlcMenuItem &editItem, QVariant* result_variant, QWidget *parent = 0);
    ~PlcEditDialog();
    void accept();

private:
    Ui::PlcEditDialog *ui;
    PlcMenuItem m_menuItem;
    QVariant m_variant;
    QVariant* m_result_variant;
    QObject *m_editor;
    QStringList m_felsorolasStoredValues;
    widgetKeyBoard *m_keyBoard;
};

#endif // PLCEDITDIALOG_H
