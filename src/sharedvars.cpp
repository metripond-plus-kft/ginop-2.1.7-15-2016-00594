#include "sharedvars.h"
#include <QtWidgets/QMessageBox>

SharedVars::SharedVars(const QString TaroloFile, ParMap* DefaultParMap): 
   FileName(TaroloFile)
{
  V=*DefaultParMap;
}

void SharedVars::Merge(ParMap *S)
{
    //V:default; S:Beolvasott
    // csak lockolt helyrol hivhato!
    foreach (const QByteArray &key, V.keys()) {
        if (S->find(key)!= S->end()) {
           if ((*S)[key].type()==V[key].type()) {
             V[key]=(*S)[key];
           }
           else
           {
               qDebug() << "Parmap key tipus hiba:" << key <<";"<<(*S)[key].type()<<";"<<(*S)[key].toString()<< ";" << FileName;
                QMetaType::Type vtype=static_cast<QMetaType::Type>(V[key].type());
                if ((*S)[key].convert(vtype))
                {
                    V[key]=(*S)[key];
                     qDebug() << V[key].type() << "tipusra konvertált érték:" << V[key];
                }
               else
                qDebug() << "Nem konvertálható erre a típusra:" << V[key].type();
               mergeMustSaveS=true;
           }
        }
        else
        {
            qDebug() << "Uj parmap key: " << key << ";" << FileName ;
            mergeMustSaveS=true;
        }
    }
    if (mergeMustSaveS) {
        SaveStatic();
    }
}

void SharedVars::Edit(std::function<void(ParMap *)> editrutin)
{
// folyamatos lockolas kell, hogy kozben ne lehessen modositas
    {
    QMutexLocker locker(&SMutex);
    editrutin(&V);
    SaveStatic();
    }
    emit modified();
}


bool  SharedVars::LoadStatic(ParMap *S)
{

    bool taroloOK=LoadSfromFile(S,FileName);

    if (taroloOK)
    {
     ParMap teszt;
     mergeMustSaveS=!LoadSfromFile(&teszt,QString("backup%1").arg(FileName));
     return true;
    }

    bool backupOK=LoadSfromFile(S,QString("backup%1").arg(FileName));

     if (backupOK)
     {
         mergeMustSaveS=true;
         return true;
     }

     InvalidFileDialog();
     return false;
}

bool SharedVars::LoadSfromFile(ParMap *S,const QString File)
{
    // load from file...
    QString fullfilename=QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation);
    fullfilename+="/"+File;

    QString savedHash=loadSavedHash(fullfilename);
    QString fileHash=myConvertedHash(fullfilename);
    if (fileHash.compare(savedHash)!=0)
    {
        qDebug() << "File hash error:" << File;
        return false;
    }

    QFile myFile(fullfilename);
    QDataStream in(&myFile);
    in.setVersion(QDataStream::Qt_5_3);
    if (!myFile.open(QIODevice::ReadOnly))
    {
        qDebug() << "Could not read the file:" << fullfilename << "Error string:" << myFile.errorString();
        return false;
    }

    in >> *S;
    return true;
}


void SharedVars::SaveStatic()
{
  // csak lockolt helyrol hivhato!

    SaveVtoFile(FileName);
    SaveVtoFile(QString("backup%1").arg(FileName));
}

void SharedVars::SaveVtoFile(const QString File)
{
    // csak lockolt helyrol hivhato!
      // save to file...
      QString fullfilename=QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation);
      fullfilename+="/"+File;

      QFile myFile(fullfilename);
      if (!myFile.open(QIODevice::WriteOnly))
      {
          qDebug() << "Could not write to file:" << fullfilename << "Error string:" << myFile.errorString();
          return;
      };
      QDataStream out(&myFile);
      out.setVersion(QDataStream::Qt_5_3);
      out << V;
      myFile.close();
      saveHash(fullfilename);
}


void SharedVars::saveHash (const QString &fileName)
{
   QString hashToWrite=myConvertedHash(fileName);
   QString hashFileName= fileName +"h";
   QFile hashFile(hashFileName);
   if (hashFile.open(QIODevice::WriteOnly))
   {
       QTextStream out(&hashFile);
       out << hashToWrite;
   }
}

QString SharedVars::loadSavedHash(const QString &fileName)
{
    QString savedHash;
    QString hashFileName= fileName+"h";
    QFile hashFile(hashFileName);
    if (hashFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&hashFile);
        in >> savedHash;
        return savedHash;
    }
    else
        return "xxx";
}

QByteArray SharedVars::fileChecksum(const QString &fileName)
{
    QFile f(fileName);
    if (f.open(QFile::ReadOnly)) {
        QCryptographicHash hash(QCryptographicHash::Sha1);
        if (hash.addData(&f)) {
            return hash.result();
        }
    }
    return QByteArray();// Returns empty QByteArray() on failure.
}

QString SharedVars::myConvertedHash(const QString &fileName)
{
    QByteArray key = "mplus-mpb";
    QByteArray fileHash =fileChecksum(fileName);
    return  QMessageAuthenticationCode::hash(fileHash, key, QCryptographicHash::Sha1).toHex();
}

void   SharedVars::InvalidFileDialog()
{
    QMessageBox fileBox;
          fileBox.setText(tr("%1 is invalid").arg(FileName));
          fileBox.setInformativeText(HibaUtasitas);
          fileBox.setStandardButtons(QMessageBox::Ok);
          fileBox.setDefaultButton(QMessageBox::Ok);
          fileBox.exec();
}
