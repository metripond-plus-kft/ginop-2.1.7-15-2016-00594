#ifndef PLCVARS_H
#define PLCVARS_H
#include "sharedvars.h"

class LocalParVars: public SharedVars{
    Q_OBJECT
public:
    LocalParVars(const QString TaroloFile, ParMap* DefaultParMap);
protected:
    void Initialize();

};

class TechVars: public SharedVars{
    Q_OBJECT
public:
    TechVars(const QString TaroloFile, ParMap* DefaultParMap, QString *order);
    void Edit(std::function<void(ParMap *)> editrutin);
    QByteArray PlcData;
    int PlcDataLength;
    void writePlcdataToV();
protected:
    QString *orderarray;
    void Initialize();
    int getVDataLength();
    void writeVtoPlcdata();
};

class PlcMemoryVars: public TechVars{
    Q_OBJECT
public:
    PlcMemoryVars(const QString TaroloFile, ParMap* DefaultParMap, QString *order);
protected:
//    void Initialize();
    bool LoadStatic(){return true;}
    void SaveStatic(){;}
};


#endif // PLCVARS_H
