#include "s7plc.h"

//a nem 1 hosszu valtozok PAROS cimen kell kezdodjenek!!!!!
static QString
orderTechParMap[]
{
    "4_Adagsuly",
    "4_Finomtomeg",
    "1_Uthtanulas",
    "1_Taragyakorisag",
    "4_Tarahatar",
    "1_Hibatures",
    "1_Zsakfogasido",
    "1_Zsaklekesl",
    "1_Tolozarkesl",
    "1_Ininvmaszk",
    "1_Outinvmaszk",
    "end"
},

orderScaleToPlcVars[]
{
    "1_Life",
    "1_Scaleflags",
    "4_Tomeg",
    "1_Parancs",
    "end"
},

orderSetPLCStoredParMap[]
{                               //etekek PLC-ben hatastalanok
    "4_Utanhullas", //csak pozitiv es 0 lehet
    "4_Muszakdb",    //csak nullazhato
    "4_Muszaksum",   //csak nullazhato
    "4_Db",          //csak nullazhato
    "4_Sum",        //csak nullazhato
    "4_Tarasuly", //ennel nagyobb lehet,
    "end"
},

orderGetPLCStoredParMap[]
{
    "4_Utanhullas",
    "4_Muszakdb",
    "4_Muszaksum",
    "4_Db",
    "4_Sum",
    "4_Tarasuly",
    "end"
},

orderPLCStateParMap[]
{
    "4_Tomeg",
    "1_Scaleflags",
    "1_Parancs",
    "4_Toltettomeg",
    "1_Prgall",
    "1_Plcflags",
    "1_In",
    "1_Out",
    "1_Tznyitkesl",
    "end"
};

static ParMap
defaultPLCParMap={
 {"Ip",(QString)"192.168.4.99"}
},

//a nem 1 hosszu valtozok PAROS cimen kell kezdodjenek!!!!!
defaultTechParMap
{
 {"4_Adagsuly",(float)50.0},
 {"4_Finomtomeg",(float)10.0},
 {"1_Uthtanulas",(bool) true},
 {"1_Taragyakorisag",(uchar)1},
 {"4_Tarahatar",(float)1.0},
 {"1_Hibatures",(uchar)15},
 {"1_Zsakfogasido",(uchar)10},
 {"1_Zsaklekesl",(uchar)10},
 {"1_Tolozarkesl",(uchar)10},
 {"1_Ininvmaszk",(uchar)0},
 {"1_Outinvmaszk",(uchar)0}
},

defaultScaleToPlcVars
{
    {"1_Life",(uchar)0},
    {"1_Scaleflags",(uchar)0},
    {"4_Tomeg",(float)0.0},
    {"1_Parancs",(uchar)0}
},

defaultSetPLCStoredParMap
{                               //etekek PLC-ben hatastalanok
    {"4_Utanhullas",(float)-1.0}, //csak pozitiv es 0 lehet
    {"4_Muszakdb",(quint32)1},    //csak nullazhato
    {"4_Muszaksum",(float)1.0},   //csak nullazhato
    {"4_Db",(quint32)1},          //csak nullazhato
    {"4_Sum",(float)1.0},         //csak nullazhato
    {"4_Tarasuly",(float)-1000000.0} //ennel nagyobb lehet
},

defaultGetPLCStoredParMap
{
    {"4_Utanhullas",(float)0.0},
    {"4_Muszakdb",(quint32)0},
    {"4_Muszaksum",(float)0.0},
    {"4_Db",(quint32)0},
    {"4_Sum",(float)0.0},
    {"4_Tarasuly",(float)0.0} //ennel nagyobb lehet
},

defaultPLCStateParMap
{
    {"4_Tomeg",(float)0.0},
    {"1_Scaleflags",(uchar)0},
    {"1_Parancs",(uchar)0},
    {"4_Toltettomeg",(float)0.0},
    {"1_Prgall",(uchar)0},
    {"1_Plcflags",(uchar)0},
    {"1_In",(uchar)0},
    {"1_Out",(uchar)0},
    {"1_Tznyitkesl",(uchar)0}
};

S7plc::S7plc(QObject *parent):
 QObject(parent),
 PlcPars("PlcVars.dat",&defaultPLCParMap),
 TechPars("TechVars.dat",&defaultTechParMap,orderTechParMap),
 ScaleToPlcVars("-",&defaultScaleToPlcVars,orderScaleToPlcVars),
 SetPlcStoredVars("-",&defaultSetPLCStoredParMap,orderSetPLCStoredParMap),
 GetPlcStoredVars("-",&defaultGetPLCStoredParMap,orderGetPLCStoredParMap),
 PlcStateVars("-",&defaultPLCStateParMap,orderPLCStateParMap),
 timer_s7(new QTimer(this)),
 timer_sec(new QTimer(this)),
 timer_250ms(new QTimer(this))
{
    connect(timer_sec, SIGNAL(timeout()), this, SLOT(secProcess()));
    connect(timer_250ms, SIGNAL(timeout()), this, SLOT(ms250Process()));
    connect(timer_s7, SIGNAL(timeout()), this, SLOT(s7ComProcess()));
    Hibauzenet="";
    m_readplcstoredvars=false;
    m_newsetplcstoredpars=false;
    m_newtechvars=false;
    m_parancs=NINCS;
    ComState=NOCONNECTION;
    m_commatempt=0;
}

S7plc::~S7plc()
{
    timer_s7->stop();
    timer_250ms->stop();
    timer_sec->stop();
    S7Client.Disconnect();
}

void S7plc::start(ScaleDisplay1 *scaledisplay)
{
    m_scaledisplayptr=scaledisplay;
    timer_sec->start(1000);
    timer_250ms->start(250);
    timer_s7->setInterval(10);
    timer_s7->setSingleShot(true);
    timer_s7->start();
}

void S7plc::ms250Process()
{
   static char x250ms=0;
   if (x250ms++<7)
    m_readplcdata=true;
   else
   {
       x250ms=0;
       m_readplcstoredvars=true;
   }
}

void S7plc::clearError()
{
    if (ComState==ERROR)
    {
        m_commatempt=0;
        ComState=NOCONNECTION;
        return;
    }
    m_parancs=STOP;
}

void S7plc::clearMuszak()
{
//    SetPlcStoredVars.V=defaultSetPLCStoredParMap;
    SetPlcStoredVars.Edit([&] (ParMap *pm)
    {(*pm)=defaultSetPLCStoredParMap;
     (*pm)["4_Muszakdb"]=(quint32)0;
     (*pm)["4_Muszaksum"]=(float)0.0;
    });
    m_newsetplcstoredpars=true;
}

void S7plc::secProcess()
{
    if (Hibauzenet=="")
       Hibauzenet=tr("PLC timeout");
     ComState=ERROR;
     emit sendHibaUzenet(Hibauzenet +tr(" (%1 comm. hiba)").arg(m_commatempt));
}

void S7plc::setComOk()
{
    timer_sec->start(1000);  //ujrainditas
    Hibauzenet="";
    m_commatempt=0;
}


bool S7plc::connectToPlcc()
{
    timer_s7->stop();
    S7Client.Disconnect();
    ComState=NOCONNECTION;
    QByteArray inUtf8 = PlcPars.GetValue()["Ip"].toString().toUtf8();
    int res=S7Client.ConnectTo(inUtf8.constData(),0,1);
    if (res==0)
        setComOk();
    else
      Hibauzenet=tr("PLC kapcsolódás sikertelen!");
    return (res==0);
}

bool S7plc::writeTechParsToPlc()
{
    if (S7Client.Connected())
    {
      int res=S7Client.DBWrite(1,0,TechPars.PlcDataLength,TechPars.PlcData.data());
      if (res==0)
          setComOk();
      else
        Hibauzenet=tr("PLC paraméter írási hiba!");
      return (res==0);
    }
    else
    {
        ComState=NOCONNECTION;
        return false;
    }
}

bool S7plc::writeScaletoPlcVarsToPlc()
{
    if (S7Client.Connected())
    {
      int res=S7Client.DBWrite(2,0,ScaleToPlcVars.PlcDataLength,ScaleToPlcVars.PlcData.data());
      if (res==0)
          setComOk();
      else
        Hibauzenet=tr("Mérleg adat PLC-be írás hiba!");
      return (res==0);
    }
    else
    {
        ComState=NOCONNECTION;
        return false;
    }
}



bool S7plc::writeSetPlcStoredVarsToPlc()
{
    if (S7Client.Connected())
    {
      int res=S7Client.DBWrite(3,0,SetPlcStoredVars.PlcDataLength,SetPlcStoredVars.PlcData.data());
      if (res==0)
          setComOk();
      else
        Hibauzenet=tr("PLC tárolt változó írás hiba!");
      return (res==0);
    }
    else
    {
        ComState=NOCONNECTION;
        return false;
    }
}


bool S7plc::readPlcStateVarsFromPlc()
{
    if (S7Client.Connected())
    {
      int res=S7Client.DBRead(4,0,PlcStateVars.PlcDataLength,PlcStateVars.PlcData.data());
      if (res==0)
      {
         setComOk();
         PlcStateVars.writePlcdataToV();
      }
      else
        Hibauzenet=tr("PLC változó olvasási hiba!");
      return (res==0);
    }
    else
    {
        ComState=NOCONNECTION;
        return false;
    }
}

bool S7plc::readGetPlcStoredVarsFromPlc()
{
    if (S7Client.Connected())
    {
      int res=S7Client.DBRead(5,0,GetPlcStoredVars.PlcDataLength,GetPlcStoredVars.PlcData.data());
      if (res==0)
      {
         setComOk();
         GetPlcStoredVars.writePlcdataToV();
      }
      else
        Hibauzenet=tr("PLC tárolt változó olvasási hiba!");
      return (res==0);
    }
    else
    {
        ComState=NOCONNECTION;
        return false;
    }
}


void S7plc::s7ComProcess()
{
    if (m_newtechvars)
    {
        ComState=INITIALIZATION;
        m_newtechvars=false;
    }
    switch (ComState) {
    case NOCONNECTION:
          if (connectToPlcc())
           ComState=INITIALIZATION;
        break;
    case INITIALIZATION:
          m_parancs=NINCS;
          m_readplcdata=false;
          m_readplcstoredvars=true;

          if (writeTechParsToPlc())
           ComState=ADATCSERE;
        break;
    case ADATCSERE:
          if (m_newsetplcstoredpars)
          {
              writeSetPlcStoredVarsToPlc();
              m_newsetplcstoredpars=false;
          }
          else
          {
              TomegData td=m_scaledisplayptr->getTomegData();
              m_tomeg=td.s_Brutto;
              m_scaleflags=0;
              if (td.s_Nyugalom)
               m_scaleflags=NYUGALOM;
              if (td.s_FolyamatosNyugalom)
               m_scaleflags|=FOLYNYUG;
              if (td.s_FinomNulla)
               m_scaleflags|=FNULLA;
              if (td.s_Alulterhelt)
               m_scaleflags|=ALULT;
              if (td.s_Tulterhelt)
               m_scaleflags|=TULT;
              if (m_auto)
               m_scaleflags|=AUTO;
              ScaleToPlcVars.Edit([&] (ParMap *pm)
              {(*pm)["1_Life"]=(uchar)(((*pm)["1_Life"].toUInt()+1) & 0xff);
               (*pm)["4_Tomeg"]=m_tomeg;
               (*pm)["1_Scaleflags"]=m_scaleflags;
               (*pm)["1_Parancs"]=m_parancs;
              });
              writeScaletoPlcVarsToPlc();
          }

          if (m_readplcstoredvars)
          {
              m_readplcstoredvars=false;
              readGetPlcStoredVarsFromPlc();
          }
          else if (m_readplcdata)
          {
              m_readplcdata=false;
              if (readPlcStateVarsFromPlc())
              {
                 if (PlcStateVars.GetValue()["1_Prgall"].toUInt()==128)
                  ComState=NOCONNECTION;
                 if (PlcStateVars.GetValue()["1_Parancs"].toUInt()==(128 | m_parancs))
                  m_parancs=0;
                emit newPlcdata();
              }
          }
        break;
    case ERROR:
    default:
        break;
    }

    if (ComState!=ERROR)
    {
        if ((++m_commatempt % 7)==0)
        {
            ComState=NOCONNECTION;
        }
    }
    timer_s7->start();
}

