#include "plcparmenu.h"
#include <QString>
#include <QtMath>

bool PlcMenuItem::isIntType(QMetaType::Type vtype) const
{
 return (vtype==QMetaType::Char) || (vtype==QMetaType::UChar) || (vtype==QMetaType::SChar)
        || (vtype==QMetaType::Short) || (vtype==QMetaType::UShort)
        || (vtype==QMetaType::Int) || (vtype==QMetaType::UInt)
        || (vtype==QMetaType::Long) || (vtype==QMetaType::ULong)
        || (vtype==QMetaType::LongLong) || (vtype==QMetaType::ULongLong);
}

QString PlcMenuItem::VariantToString(const QVariant variant, const int displayedDecimal) const
{
  if (variant.type()==QVariant::Bool)
   return  variant.toBool() ? QObject::tr("igen") : QObject::tr("nem");
  QMetaType::Type vtype=static_cast<QMetaType::Type>(variant.type());
  if ((vtype==QMetaType::Float) || (variant.type()==QVariant::Double))
   return QString::number(variant.toDouble(),'f',displayedDecimal);
  if (isIntType(vtype))
  {
    double dvalue=variant.toDouble()*qPow(10,displayedDecimal*-1);
    return QString::number(dvalue,'f',displayedDecimal);
  }
  return variant.toString();
}

QStringList PlcMenuItem::getFelsorolasDisplayValues(ParMap *Felsorolas, const int displayedDecimal) const
{
  QStringList result;
  for (ParMap::iterator i=Felsorolas->begin() ;i!=Felsorolas->end();i++)
      result+=VariantToString(i.value(),displayedDecimal);
  return result;
}

QStringList PlcMenuItem::getFelsorolasStoredValues(ParMap *Felsorolas) const
{
  QStringList result;
  for (ParMap::iterator i=Felsorolas->begin() ;i!=Felsorolas->end();i++)
      result+=i.value().toString();
  return result;
}

double PlcMenuItem::getMax() const
{
    if (Max==MAXTOMEG)
    {
        return (*(Owner->getCalpmptr()))["Tartomanyszam"]==2 ? (*(Owner->getCalpmptr()))["Mereshatar2"].toDouble() : (*(Owner->getCalpmptr()))["Mereshatar1"].toDouble();
    }
     else
        return Max;
}

QString PlcMenuItem::UnitStr() const
{
    if (UnitId=="*")
    {
        return (*(Owner->getCalpmptr()))["Mertekegyseg"].toString();
    }
    else
        return PlcMenuTableModel::UnitMap[UnitId].toString();
}

int PlcMenuItem::getDecimal() const
{
    if (Tizedes>=0)
     return Tizedes;
    return (*(Owner->getCalpmptr()))["Tomegtizedes"].toInt();
}

PlcMenuTableModel::PlcMenuTableModel(const int fomenuItem, S7plc *Plcptr):
 m_Pp(Plcptr->getPp()),  //PlcPars - kapcs. parameterek
 m_Tp(Plcptr->getTp()),  //TechPars - tech. parameterek
 m_Spsv(Plcptr->getSpsv()), //SetPlcStoredVars - letoltendo, PLC-ben tarolt adatok
 m_Psv(Plcptr->getPsv())  //PlcStateVars - Plc allapot
{
    m_calpm=Plcptr->getScaleDisplayptr()->getCalibrationMap();
    inicPlcMenu();
    inicTechMenu();
    inicAdagMenu();
    inicFoMenu();
    selectMenu(fomenuItem);
}


ParMap PlcMenuTableModel::UnitMap
{
    {"-",(QString)QObject::tr(" ")},
    {"g",(QString)QObject::tr("g")},
    {"kg",(QString)QObject::tr("kg")},
    {"t",(QString)QObject::tr("t")},
    {"ms",(QString)QObject::tr("ms")},
    {"s",(QString)QObject::tr("s")},
    {"min",(QString)QObject::tr("min")},
    {"h",(QString)QObject::tr("h")},
    {"4_Db",(QString)QObject::tr("db")},
    {"t/h",(QString)QObject::tr("t/h")},
    {"d",(QString)QObject::tr("d")},
};

ParMap PlcMenuTableModel::IgenNemList
{
    {"Igen",(bool)true},
    {"Nem",(bool)false}
};

void PlcMenuTableModel::inicPlcMenu()
{
 int index=0;
    PlcMenu.insert(index++,{this,QObject::tr("IPv4 cím"),m_Pp,"Ip","-",0,0,0,nullptr,EDITDIALOG});
}

void PlcMenuTableModel::inicTechMenu()
{
  int index=0;
    TechMenu.insert(index++,{this,QObject::tr("Utánhullás tanulás"),m_Tp,"1_Uthtanulas","-",0,0,0,&IgenNemList,EDITDIALOG});
    TechMenu.insert(index++,{this,QObject::tr("Üres súly mérés gyak."),m_Tp,"1_Taragyakorisag","db",0,0,255,nullptr,EDITDIALOG});
    TechMenu.insert(index++,{this,QObject::tr("Üres súly határ"),m_Tp,"4_Tarahatar","*",-1,0,MAXTOMEG,nullptr,EDITDIALOG});
    TechMenu.insert(index++,{this,QObject::tr("Hibatűrési idő"),m_Tp,"1_Hibatures","s",1,0.0,25.5,nullptr,EDITDIALOG});
    TechMenu.insert(index++,{this,QObject::tr("Zsákfogás idő"),m_Tp,"1_Zsakfogasido","s",1,0.0,25.5,nullptr,EDITDIALOG});
    TechMenu.insert(index++,{this,QObject::tr("Zsákleadás késl."),m_Tp,"1_Zsaklekesl","s",1,0.0,25.5,nullptr,EDITDIALOG});
    TechMenu.insert(index++,{this,QObject::tr("Tolózár nyitás késl."),m_Tp,"1_Tolozarkesl","s",1,0.0,25.5,nullptr,EDITDIALOG});
    TechMenu.insert(index++,{this,QObject::tr("Input inv. maszk"),m_Tp,"1_Ininvmaszk","-",0,0,255,nullptr,EDITDIALOG});
    TechMenu.insert(index++,{this,QObject::tr("Output inv. maszk"),m_Tp,"1_Outinvmaszk","-",0,0,255,nullptr,EDITDIALOG});
}

void PlcMenuTableModel::inicAdagMenu()
{
    int index=0;
    AdagMenu.insert(index++,{this,QObject::tr("Adagsúly"),m_Tp,"4_Adagsuly","*",-1,0,MAXTOMEG,nullptr,EDITDIALOG});
    AdagMenu.insert(index++,{this,QObject::tr("Finom tömeg"),m_Tp,"4_Finomtomeg","*",-1,0,MAXTOMEG,nullptr,EDITDIALOG});
    AdagMenu.insert(index++,{this,QObject::tr("Utánhullás"),m_Spsv,"4_Utanhullas","*",-1,0,MAXTOMEG,nullptr,EDITDIALOG});
    AdagMenu.insert(index++,{this,QObject::tr("Üres zsák súly"),m_Spsv,"4_Tarasuly","*",-1,0,MAXTOMEG,nullptr,EDITDIALOG});
}

void PlcMenuTableModel::inicFoMenu()
{
 m_foMenu.insert(0,{QObject::tr("Adagolás"),&AdagMenu,AdagMenu.size()});
 m_foMenu.insert(1,{QObject::tr("Paraméterek"),&TechMenu,TechMenu.size()});
 m_foMenu.insert(2,{QObject::tr("Plc kapcsolat"),&PlcMenu,PlcMenu.size()});
}

int PlcMenuTableModel::rowCount(const QModelIndex& parent) const
{
  Q_UNUSED(parent);
  return(m_count);
}

int PlcMenuTableModel::columnCount(const QModelIndex& parent) const
{
  Q_UNUSED(parent);
  return(3);
}

QVariant  PlcMenuTableModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant::Invalid;

    if (index.row() >= m_count)
        return QVariant::Invalid;

    if(role == Qt::DisplayRole)
      {
        switch (index.column()) {
        case 0:
            return m_menuMapPtr->value(index.row()).NevStr;
            break;
        case 1:{
            ParMap pm(m_menuMapPtr->value(index.row()).Tarolo->GetValue());
            return m_menuMapPtr->value(index.row()).VariantToString(pm[m_menuMapPtr->value(index.row()).ValtozoId],m_menuMapPtr->value(index.row()).getDecimal()); //.toString();
            break;}
        case 2:
            return m_menuMapPtr->value(index.row()).UnitStr();
            break;
        default:
             return QVariant::Invalid;
            break;
        };
      }
    return QVariant::Invalid;

}


QVariant  PlcMenuTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

   if (orientation==Qt::Horizontal)
    switch (section) {
    case 0:
        return QObject::tr("Paraméter");
        break;
    case 1:
        return QObject::tr("Érték");
        break;
    case 2:
        return QObject::tr("M.e.");
        break;
    default:
        return QVariant::Invalid;
        break;
    }
   else
    return QVariant();
}

Qt::ItemFlags PlcMenuTableModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

bool PlcMenuTableModel::setData(const QModelIndex &index, const QVariant &value, int role /*=Qt::EditRole*/)
{
    Q_UNUSED(index);
    Q_UNUSED(value);
    Q_UNUSED(role);
 return true;
}

void PlcMenuTableModel::selectMenu(const int menuId)
{
    if ((menuId<0) || (menuId>=m_foMenu.count()))
      return;

    m_FomenuItemId=menuId;
    m_menuMapPtr=m_foMenu[menuId].MenuMapPtr;
    m_count=m_foMenu[menuId].Count;
}

QStringList PlcMenuTableModel::getFomenuNames()
{
  QStringList result;
  for (int i=0;i<m_foMenu.count();i++)
        result+=m_foMenu[i].NevStr;
  return result;
}


PlcMenuItem PlcMenuTableModel::getMenuItem(const QModelIndex& index)
{
  if (!index.isValid())
   return (m_menuMapPtr->value(0));
  return (m_menuMapPtr->value(index.row()));
}
